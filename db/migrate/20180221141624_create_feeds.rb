class CreateFeeds < ActiveRecord::Migration[5.1]
  def change
    create_table :feeds do |t|
      t.string :fecha
      t.string :title
      t.string :image
      t.string :content
      t.text :all_content
      t.string :link
      t.string :comments
      t.string :categories

      t.timestamps
    end
  end
end

class AddLikeToFeed < ActiveRecord::Migration[5.1]
  def change
    add_column :feeds, :like, :integer, default: 0
  end
end

class FeedsController < ApplicationController
	before_action :set_feed, only: [:show, :like_count]
  
  def index
  	@feeds = Feed.all
  end

	def show
		@comments = Comment.where(feed_id: params[:id])

	end

	def like_count

    @feed.like += 1
    @feed.save
    redirect_to feed_path(@feed), notice: "Like succsess"
     

	end

	private
	    # Use callbacks to share common setup or constraints between actions.
	    def set_feed
	      @feed = Feed.find(params[:id])
	    end

end

class CommentsController < ApplicationController
  def create
  	if params[:name].present? && params[:email].present?
  	comment = Comment.new
  	comment.name = params[:name]
  	comment.email = params[:email]
  	comment.comment = params[:comment]
  	comment.feed_id = params[:feed_id]
  	comment.save
  else 
  	comment = Comment.new
  	comment.name = "Anónimo"
  	comment.email = "anonimo@anonimo.cl"
  	comment.comment = params[:comment]
  	comment.feed_id = params[:feed_id]
  	comment.save
    end

    redirect_to feed_path(params[:feed_id]), notice: 'Comentario Agregado'
  end

  def index
    
  end


end